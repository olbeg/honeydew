var Hello = React.createClass({ render: function() {
        return (<div>
                <h3>Hello {this.props.name} {this.props.date}</h3>
                </div>);
    }
});

$(function(){
    setInterval(function() {
        $.ajax({
            url: "http://azazash.tk:8000/timestamp",
            context: document.body,
            crossDomain: true,
            dataType: 'jsonp',
            error: function() {
                console.log("fail");
            },
            success: function(data) {
                React.render(
                <Hello name="World" date={data.date} />,
                document.getElementsByClassName('b-page__head-line')[0])
            }
        });
        $.post("http://azazash.tk:8000/test", function(data) {
            console.log(data);
        });
    }, 500);
}());

//load image from imgur.com
var Imgur = React.createClass({
    render: function() {
        return (<img src={this.props.img}></img>)
    }
});

React.render(<Imgur img={'//i.imgur.com/ztjn2ms.jpg'} />,
             document.getElementsByClassName('b-page__footer')[0]);

//set body background image with imgur API
function setBackgroundImage(links, counter) {
            $('.b-page').css({'background': 'url(' + links[counter] + ')',
                              'background-size': 'cover',});
            counter++;
        };

$.ajax({
    type: "GET",
    url: "https://api.imgur.com/3/album/Cfy6A/images",
    dataType: "text",
    beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', 'Client-ID e9f7b38b5fe1cfd');
    },
    success: function(data) {
        var json = $.parseJSON(data);
        var links = json.data.map(function(item) {
            return item.link;
        });
        var counter = 0;
        setBackgroundImage(links, counter);
        setInterval(setBackgroundImage(links, counter), 10002);
    }
});
